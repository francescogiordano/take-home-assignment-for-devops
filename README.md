## Take Home Assignment for DevOps
### Cloud Data Platform Admin CLI
Design of an SRE general tool __CDPAdminCli__ in Python. The tool should be in format of a linux-like __Command Line Interface__. __CDPAdminCli__'s main purpose be the one of supporting the SRE team to work with a remote _cloud data platform_ in general administrative tasks, such as: starting / stopping clusters, administrating users, getting general and detailed info about metrics, etc... 

Apply the following considerations while doing what above:

### Functionalities
For the purpose of the excercise, it is not particularly important to indicate functionalities for the tool that make really sense in the context. Some examples would suffice.

### Modularity
The tool should be designed with __modularity__ as a key aspect. It should be conceived as with a plugin-like structure, so that additional functionalities could be easily added to the tool (possibly by different member of the team and in parallel).

### Separation of concerns
The tool should present clear rules of extensions. It should be clear where to add documentation, code, modules, etc... for new commands, relying on __dependency injection__ whenever necessary.

### Coding
It is not mandatory to code much. Only modules' structure is essential. However, some complete modules would be appreciated as well as a general indication of the required libraries. Feel free to fork the project to commit here your work.

### Testing
To demonstrate a good mastering of TDD technique, define the general structure of the tests and make sure that at least one test case is present for each implemented module in the final deliverable.
